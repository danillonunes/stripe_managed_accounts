<?php

/**
 * Returns structure from Stripe API Account object.
 */
function stripe_managed_accounts_api_schema_account() {
  return array(
    'id' => '',
    'object' => '',
    'business_logo' => '',
    'business_name' => '',
    'business_primary_color' => '',
    'business_url' => '',
    'charges_enabled' => '',
    'country' => '',
    'currencies_supported' => '',
    'debit_negative_balances' => '',
    'decline_charge_on' => array(
      'avs_failure' => '',
      'cvc_failure' => '',
    ),
    'default_currency' => '',
    'details_submitted' => '',
    'display_name' => '',
    'email' => '',
    'external_account' => array(),
    'external_accounts' => array(),
    'legal_entity' => array(
      'additional_owners' => array(),
      'address' => array(
        'city' => '',
        'country' => '',
        'line1' => '',
        'line2' => '',
        'postal_code' => '',
        'state' => '',
      ),
      'business_name' => '',
      'dob' => array(
        'year' => '',
        'month' => '',
        'day' => '',
      ),
      'first_name' => '',
      'last_name' => '',
      'personal_address' => array(
        'city' => '',
        'country' => '',
        'line1' => '',
        'line2' => '',
        'postal_code' => '',
        'state' => '',
      ),
      'personal_id_number_provided' => '',
      'ssn_last_4_provided' => '',
      'type' => '',
      'verification' => array(
        'details' => '',
        'details_code' => '',
        'document' => '',
        'status' => '',
      ),
    ),
    'managed' => '',
    'metadata' => array(),
    'product_description' => '',
    'statement_descriptor' => '',
    'support_email' => '',
    'support_phone' => '',
    'support_url' => '',
    'timezone' => '',
    'tos_acceptance' => array(
      'date' => '',
      'ip' => '',
      'user_agent' => '',
    ),
    'transfer_schedule' => array(
      'delay_days' => '',
      'interval' => '',
      'monthly_anchor' => '',
      'weekly_anchor' => '',
    ),
    'verification' => array(
      'disabled_reason' => '',
      'due_by' => '',
      'fields_needed' => array(),
    ),
  );
}

/**
 * Returns a list of Stripe supported countries.
 */
function stripe_managed_accounts_api_schema_countries() {
  return array(
    'AU' => t('Australia'),
    'CA' => t('Canada'),
    'DE' => t('Denmark'),
    'FI' => t('Finland'),
    'IE' => t('Ireland'),
    'NO' => t('Norway'),
    'SW' => t('Sweden'),
    'UK' => t('United Kingdom'),
    'US' => t('United States'),
  );
}

/**
 * Returns a list of Stripe supported currencies for external accounts, by countries.
 */
function stripe_managed_accounts_api_schema_external_account_currencies() {
  return array(
    'AU'=> array(
      'AUD' => t('Australian Dollar'),
    ),
    'CA'=> array(
      'AUD' => t('Canadian Dollar'),
      'USD' => t('United States Dollar'),
    ),
    'DE'=> array(
      'DKK' => t('Danish Krone'),
      'NOK' => t('Norwegian Krone'),
      'SEK' => t('Swedish Krona'),
      'EUR' => t('Euro'),
      'GBP' => t('British Pound'),
      'USD' => t('United States Dollar'),
    ),
    'FI'=> array(
      'DKK' => t('Danish Krone'),
      'NOK' => t('Norwegian Krone'),
      'SEK' => t('Swedish Krona'),
      'EUR' => t('Euro'),
      'GBP' => t('British Pound'),
      'USD' => t('United States Dollar'),
    ),
    'IE'=> array(
      'EUR' => t('Euro'),
      'GBP' => t('British Pound'),
      'USD' => t('United States Dollar'),
    ),
    'NO'=> array(
      'DKK' => t('Danish Krone'),
      'NOK' => t('Norwegian Krone'),
      'SEK' => t('Swedish Krona'),
      'EUR' => t('Euro'),
      'GBP' => t('British Pound'),
      'USD' => t('United States Dollar'),
    ),
    'SE'=> array(
      'DKK' => t('Danish Krone'),
      'NOK' => t('Norwegian Krone'),
      'SEK' => t('Swedish Krona'),
      'EUR' => t('Euro'),
      'GBP' => t('British Pound'),
      'USD' => t('United States Dollar'),
    ),
    'UK'=> array(
      'EUR' => t('Euro'),
      'GBP' => t('British Pound'),
      'USD' => t('United States Dollar'),
    ),
    'US'=> array(
      'USD' => t('United States Dollar'),
    ),
  );
}
